import itertools
import sys
import os
from nltk.stem import WordNetLemmatizer

def getFuzzyLemmaSenses(lemma, lemmas2vectors_map):

	# Returns a list of Matches, where each Match is a list itself.
	# A Match may have more than one elements when there are more than one vectors for the same lexicalization.
	#
	# e.g: [In] getFuzzyLemmaSenses('apple', lemmas2vectors)
	#	   [Out] [[u'napple_tale_bn:03228159n'], [u'appleton_bn:00730066n',u'appleton_bn:00005084n',u'appleton_bn:00292767n' ... ] ... ]
	matches = [val for key, val in lemmas2vectors_map.iteritems() if lemma in key]
	return [i[0] for i in matches]

def getLemmaSenses(lemma, lemmas2vectors_map):

	if lemma.lower() in lemmas2vectors_map:
		return lemmas2vectors_map[lemma.lower()]

def filter_senses_by_pos(list_of_senses, input_pos):

	out = []
	for elm in list_of_senses:

		if elm[-1] == input_pos:

			out.append(elm)
	return out

def closest_senses(list_of_senses1, list_of_senses2, model):

	max_sim = -9999

	for s1 in list_of_senses1:
		for s2 in list_of_senses2:
			sim = model.similarity(s1, s2)
			if sim > max_sim:
				best_pair = (s1,s2)
				max_sim = sim
	return best_pair[0],best_pair[1],max_sim

def lexicalizations_similarity(lexis1, lexis2, model, model_vocab):

	if lexis1 in model_vocab and lexis2 in model_vocab:

		lexis1_senses = getLemmaSenses(lexis1)
		lexis2_senses = getLemmaSenses(lexis2)

		print lexis1_senses
		print lexis2_senses

		max_sim = -9999
		closest_senses = ["", ""]

		for s1 in lexis1_senses:

			for s2 in lexis2_senses:

				if not s1 == s2:

					sim = model.similarity(s1, s2)

					if sim > max_sim:

						max_sim = sim
						closest_senses[0] = s1
						closest_senses[1] = s2

		return closest_senses

def best_sense_of_many(list_of_senses, sensembed_model_vocab):

	# list_of_senses -> list of lists, where each embedded list contains all possible senses retrieved for a given lemma
	# Returns a new_list, where len(new_list) == len(list_of_senses), but new_list is a list of sensembed vectors.
	# These "best" sensembed vectors are obtained by doing sensembed_model.

	for vec in sensembed_model_vocab:

		pass

if __name__ == '__main__':
	pass