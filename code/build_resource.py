import os
import sys
from collections import defaultdict as dd

magn_thss = ('magn',0.7) # superstrict
magn_ths = ('magn',0.5) # strict
magn_thr = ('magn',0.35) # relaxed

antimagn_thss = ('antimagn',0.45) # superstrict
antimagn_ths = ('antimagn',0.25) # strict
antimagn_thr =('antimagn',0.125)

oper_thss = ('oper',0.5) # superstrict
oper_ths = ('oper',0.35) # strict
oper_thr = ('oper',0.25)

causfunc_thss = ('causfunc',0.45) # superstrict
causfunc_ths = ('causfunc',0.3) # strict
causfunc_thr = ('causfunc',0.15)

causpredplus_thss = ('causpredplus',0.5) # superstrict
causpredplus_ths = ('causpredplus',0.35) # strict
causpredplus_thr = ('causpredplus',0.175)

causpredminus_thss = ('causpredminus',0.5) # superstrict
causpredminus_ths = ('causpredminus',0.35) # strict
causpredminus_thr =('causpredminus',0.175)

liqufunc_thss = ('liqufunc',0.4) # superstrict
liqufunc_ths = ('liqufunc',0.25) # strict
liqufunc_thr = ('liqufunc',0.125)

incepoper_thss = ('incepoper',0.25) # superstrict
incepoper_ths = ('incepoper',0.15) # strict
incepoper_thr = ('incepoper',0.075)

manif_thss = ('manif',0.2) # superstrict
manif_ths = ('manif',0.1) # strict
manif_thr = ('manif',0.05)

finoper_thss = ('finoper',0.1) # superstrict
finoper_ths = ('finoper',0.05) # strict
finoper_thr = ('finoper',0)

all_th = [magn_ths, antimagn_thss, oper_thss, causfunc_thss, causpredplus_thss, causpredminus_thss,\
liqufunc_thss, incepoper_thss, manif_thss, finoper_thss]	

if __name__ == '__main__':

	args = sys.argv[1:]

	if len(args) == 2:

		folder = args[0] # folder where wordnet relations are stored, y default cwn/output
		cwn_file = args[1] # path for the resulting CWN resource

		listing = os.listdir(folder)
		out = dd(lambda : dd(float))
		outf = open(cwn_file, 'w')
		for infile in listing:
			file_fl = infile.split('.txt')[0]
			for item in all_th:
				ref_fl = item[0]
				threshold = item[1]
				if file_fl.lower() == ref_fl: # apply given threshold only in its corresponding file
					print 'Processing file: ',infile
					print 'With threshold: ',threshold
					print '-----'
					f = open(folder+infile, 'r')
					ls = f.readlines()
					blocks = f.read().split('\n\n')
					for line in ls:
						#print line
						if len(line) > 1:
							cols = line.strip().split('\t')
							if len(cols) == 3:
								base = cols[0]
								candidate = cols[1]
								score = float(cols[2])
								if score > threshold:
									print 'Adding -> ',base,' -> ',candidate,' -> ',score
									out[base][candidate+'__'+ref_fl] += score
		rel_counter = 0
		for base in out:
			cands = out[base]
			sorted_cands = sorted(cands.items(), key = lambda x:x[1], reverse=True)
			for c in sorted_cands:
				collocate = c[0].split('__')[0]
				type_of_relation = c[0].split('__')[1]
				rel_counter += 1
				outf.write(base+'\t'+type_of_relation+'\t'+collocate+'\t'+str(c[1])+'\n')
			outf.write('\n')
		outf.close()
		print 'Number of relations added: ',rel_counter