import sys
import networkx as nx
import nltk
from nltk.corpus import wordnet as wn
import json

# Initialize custom WN container as a directed graph
G = nx.DiGraph()

def types_of(input_synset):
	# Return all hyponyms for an input synset
	out = [s for s in input_synset.closure(lambda s:s.hyponyms())]
	return out

def add_rels(syn):
	# Add hyponym, hypernym relations
	for h in syn.hypernyms():
		G.add_edge(syn, h, {'relation':'hypernym'})
	for h in syn.instance_hypernyms():
		G.add_edge(syn, h, {'relation':'instance_hypernym'})
	for h in syn.hyponyms():
		G.add_edge(syn, h, {'relation':'hyponyms'})
	for h in syn.instance_hyponyms():
		G.add_edge(syn, h, {'relation':'instance_hyponym'})
	for holo in syn.member_holonyms():
		G.add_edge(syn, holo, {'relation':'member_holonym'})
	for holo in syn.substance_holonyms():
		G.add_edge(syn, holo, {'relation':'substance_holonym'})
	for holo in syn.part_holonyms():
		G.add_edge(syn, holo, {'relation':'part_holonym'})
	for m in syn.member_meronyms():
		G.add_edge(syn, m, {'relation':'member_meronym'})
	for m in syn.substance_meronyms():
		G.add_edge(syn, m, {'relation':'substance_meronym'})
	for m in syn.part_meronyms():
		G.add_edge(syn, m, {'relation':'part_meronym'})
	for a in syn.attributes():
		G.add_edge(syn, a, {'relation':'attribute'})
	for e in syn.entailments():
		G.add_edge(syn, e, {'relation':'entailment'})
	for c in syn.causes():
		G.add_edge(syn, c, {'relation':'causes'})
	for als in syn.also_sees():
		G.add_edge(syn, als, {'relation':'also_sees'})
	for vg in syn.verb_groups():
		G.add_edge(syn, vg, {'relation':'verb_groups'})
	for sim in syn.similar_tos():
		G.add_edge(syn, sim, {'relation':'similar_to'})


def add_collocations():
	# must be aligned
	for idx,base in enumerate(all_bases):
		G.add_edge(base, all_collocates[idx], {'relation':'collocation', 'lf':all_lfs[idx], 'score':all_scores[idx]})


def bases_and_collocates(cwn_file):
	f = open(CWN_RELATIONS, 'r')
	all_bases = []
	all_lfs = []
	all_scores = []
	all_collocates = []
	for line in f:
		line = line.strip()
		if line:
			cols = line.split('\t')
			base = wn.synset(cols[0])
			lf = cols[1]
			col = wn.synset(cols[2])
			score = float(cols[3])
			all_bases.append(base)
			all_lfs.append(lf)
			all_collocates.append(col)
			all_scores.append(score)
	return all_bases,all_lfs,all_scores,all_collocates

def get_collocates(inputbase_syn, lf=""):
	# lf = {magn, antimagn, oper, finoper, incepoper, liqufunc, causpredplus, causpredminus, manif}
	if inputbase_syn in G.nodes():
		out = []
		for e in G.edges():
			edge_attrs = G.get_edge_data(e[0],e[1])
			if lf:
				if edge_attrs['relation'] == 'collocation' and edge_attrs['lf'] == lf and e[0] == inputbase_syn:
					out.append((e,edge_attrs))
			else:
				if edge_attrs['relation'] == 'collocation' and e[0] == inputbase_syn:
					out.append((e,edge_attrs))			
		return out
	else:
		sys.exit('The example synset is not in CWN:',inputbase_syn)

def get_neighbours(inputsyn, by_relation=None):
	if inputsyn in G.nodes():
		if by_relation == None:
			return G.successors(inputsyn)+G.predecessors(inputsyn)
		else:
			out = []
			for syn in G.successors(inputsyn)+G.predecessors(inputsyn):
				if G.get_edge_data(inputsyn, syn)['relation'] == by_relation:
					out.append(syn)
			return out
	else:
		sys.exit('The example synset is not in CWN:',inputsyn)

if __name__ == '__main__':

	args = sys.argv[1:]

	if len(args) == 2:
		CWN_RELATIONS = args[0]
		topmost_synset = args[1]
		# Declare top-most synset whose hyponyms are traversed to create a smaller WN (for all WN, use "entity.n.01")
		entity = wn.synset(topmost_synset)
		# Iterate over all the topmost synset's hyponyms, and populate G
		for idx,wnsyn in enumerate([entity]+types_of(entity)):
			print 'Processing: ',wnsyn,' | ',idx,' of ',len([entity]+types_of(entity))
			add_rels(wnsyn)
		# Get from CWN relations three lists of equal length for each collocation relation, consisting in:
		# base, lexical function, score, and collocate
		all_bases,all_lfs,all_scores,all_collocates = bases_and_collocates(CWN_RELATIONS)
		# Add collocations to custom WN
		add_collocations()
		print "collocations added to WordNet's subtree rooted at ",topmost_synset
	if len(args) == 3:
		CWN_RELATIONS = args[0]
		topmost_synset = args[1]
		example_synset = args[2]
		# Declare top-most synset whose hyponyms are traversed to create a smaller WN (for all WN, use "entity.n.01")
		entity = wn.synset(topmost_synset)
		# Iterate over all the topmost synset's hyponyms, and populate G
		for idx,wnsyn in enumerate([entity]+types_of(entity)):
			print 'Processing: ',wnsyn,' | ',idx,' of ',len([entity]+types_of(entity))
			add_rels(wnsyn)
		# Get from CWN relations three lists of equal length for each collocation relation, consisting in:
		# base, lexical function, score, and collocate
		all_bases,all_lfs,all_scores,all_collocates = bases_and_collocates(CWN_RELATIONS)
		# Add collocations to custom WN
		add_collocations()

		print "collocations added to WordNet's subtree rooted at ",topmost_synset
		# example in interactive terminal / notebook
		print('*** Example ***')
		input_synset = wn.synset(example_synset)
		for n in get_neighbours(input_synset, by_relation='collocation'):
			print "Synset1: %s" % n
			print "Synset2: %s" % input_synset
			print "Relation: %s" % str(G.get_edge_data(input_synset, n))
			print "------"
		#input_synset = wn.synset('spasm.n.01')
		for edge,relation in get_collocates(input_synset):
			print edge,relation
	else:
		print 'Usage: python load_CWN_relations.py cwn_file topmost_synset example_synset (optional)'
		sys.exit()