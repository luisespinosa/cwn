# -*- coding: utf-8 -*-
import json
import sys
import gensim
import os
import codecs
#import ic
import logging
import pandas as pd 
import numpy as np
from collections import defaultdict as dd
import Sensembed
from nltk.corpus import wordnet as wn

def types_of(input_synset):
	# Return all hyponyms for an input synset
	out = [s for s in input_synset.closure(lambda s:s.hyponyms())]
	return out

def most_similar_vector(self, vectenter, topn=20):
	self.init_sims(replace=True)	
	dists = np.dot(self.syn0norm, vectenter)
	if not topn:
		return dists
	best = np.argsort(dists)[::-1][:topn ]
		# ignore (don't return) words from the input
	result = [(self.index2word[sim], float(dists[sim])) for sim in best]
	return result[:topn]

def load_bn_wn(bn_wn_file):
	out = {}
	out2 = {}
	for line in open(bn_wn_file, 'r'):
		cols = line.strip().split('\t')
		bn = cols[0]
		wn = cols[1]
		out[bn] = wn
		out2[wn] = bn
	return out,out2

def get_collocates_syn(input_base_synset,collocate_pos):
	# input_base_synset -> must be wordnet synset
	out = dd(lambda : dd(list))
	base_and_collocates = dd(lambda : dd(list))
	base_sensembed_vecs = []
	synvecs = []
	s_vectors = wnSyns2SensembedVecs[input_base_synset.name()]
	s_vectors = Sensembed.filter_senses_by_pos(s_vectors, input_base_synset.pos())
	if s_vectors:
		base_sensembed_vecs.append(s_vectors)
	for vecs_set in base_sensembed_vecs:
		mapped_vec_wn = 'no_map'
		for vec in vecs_set:
			# Get the wordnet synset for the base
			vec_bn = 'bn:'+vec.split('_bn:')[1]
			if vec_bn in bn2wn:
				vec_wn = bn2wn[vec_bn]
				mapped_vec_wn = senseIdToSynset[int(vec_wn[:-1])]
				break
		for vec in vecs_set:
			#print 'Getting collocates from: ',vec.encode('utf-8')
			#res = most_similar_vector(shared_model,tm_full.dot(M[vec]))
			pred = np.array(clf.predict(M[vec]))
			#print shared_model.most_similar(positive=[pred[-1]])
			res = shared_model.most_similar(positive=[pred[-1]])
			#print 'res:',res
			for idx,r in enumerate(res):
				#print 'For input_base: ',input_base_synset,' | base vector: ',vec.encode('utf-8'),' | Res: ',r
				base_and_collocates[mapped_vec_wn][r[0]].append(r[1])#/(idx+1))
				if r[0].startswith('bn:'):
					if r[0] in wn2bn.values():
						wnsyn = bn2wn[r[0]]
						mapped_wnsyn = senseIdToSynset[int(wnsyn[:-1])]
						print 'Saving: ',(mapped_wnsyn,r[1])
						out[mapped_vec_wn][mapped_wnsyn].append(r[1])#/(idx+1))
	# base_and_collocates: {wn_synset1 : {col1:[list_of_scores1], col2:[list_of_scores2]}, wn_synset2 : ...}
	# out: {wn_synset1 : {col_wn1:[list_of_scores1], col_wn2:[list_of_scores2]}, wn_synset2 : ...}
	return out,base_and_collocates

def filter_candidates(base_and_collocates):
	out = dd(lambda : dd(float))
	# filter by accumulated score
	for syn in base_and_collocates:
		for cand in base_and_collocates[syn]:
			if type(cand) == str:
				#print 'In filter:',syn,' -> ',cand.encode('utf-8'),' -> scores: ',base_and_collocates[syn][cand]
				all_scores = []
				for score in base_and_collocates[syn][cand]:
					all_scores.append(score)
				out[syn][cand] = sum(all_scores)/float(len(all_scores))
			else:
				#print syn,' -> ',cand,' -> scores: ',base_and_collocates[syn][cand]
				all_scores = []
				for score in base_and_collocates[syn][cand]:
					all_scores.append(score)
				out[syn][cand] = sum(all_scores)/float(len(all_scores))
	sout = {}
	for syn in out:
		tosort = out[syn]
		s = sorted(tosort.items(), key=lambda x:x[1], reverse=True)
		sout[syn]=s
	return sout

if __name__ == '__main__':

	args = sys.argv[1:]

	if len(args) == 5:

		train_data = args[0]
		base_pos = args[1]
		col_pos = args[2]
		base_cpts = args[3]
		data_folder = args[4]

		for infile in os.listdir(data_folder):
			if infile=='babelfy_vectors_ids_header_bin':
				sensembed_model=data_folder+infile
			elif infile=='full_multi_webbase_synsets_u0_st1_si2.bin':
				shared_model_path=data_folder+infile
			elif infile=='lemmas2vectors_map.json':
				lemmas2vectors=data_folder+infile
			elif infile=='babelsynsets2vectors_map.json':
				bnsyn2sensembedids=data_folder+infile
			elif infile=='wnSyns2SensembedVecs.json':
				wnSyns2SensembedVecs_path=data_folder+infile
			elif infile=='all_bn_wn.txt':
				bn2wn_path=data_folder+infile

		fl = train_data.split('/')[-1]

		print 'Loading mapping between BabelNet synsets and Sensembed vector ids...'
		bnsyns2vectors = json.load(open(bnsyn2sensembedids, 'r'))
		print 'Loading mapping between lemmas and Sensembed vector ids...'
		lemmas2vectors = json.load(open(lemmas2vectors, 'r'))
		print 'Loading WN <-> Sensembed map...'
		wn_se_map = wnSyns2SensembedVecs_path
		wnSyns2SensembedVecs = json.load(open(wn_se_map, 'r'))

		print 'Loading Sensembed...'
		M = gensim.models.Word2Vec.load_word2vec_format(sensembed_model, binary=True)
		M_vocab = set(M.index2word)
		M.init_sims(replace=True)

		print 'Loading SW2V Space...'
		shared_model = gensim.models.Word2Vec.load_word2vec_format(shared_model_path, binary=True)
		shared_vocab = set(shared_model.index2word)
		shared_model.init_sims(replace=True)

		print 'Building wnsynset2offset map'
		all_syns = wn.all_synsets()
		senseIdToSynset = {s.offset():s for s in all_syns}
		all_syns = ''

		print 'Loading mapping between Babelnet and Wordnet...'
		bn2wn,wn2bn = load_bn_wn(bn2wn_path)

		print 'Reading training pairs...'
		word_pairs = codecs.open(train_data, 'r', 'utf-8')
		outf_disambiguated = codecs.open(train_data+'_disamb_sensembed+shared.csv', 'w', 'utf-8')
		outf_disambiguated.write('source|target|score\n')
		pairs = pd.read_csv(word_pairs)
		all_train_vecs = dd(list)
		for i in range(len(pairs)):
			base_senses = Sensembed.getLemmaSenses(pairs['source'][i], lemmas2vectors)
			col_senses = Sensembed.getLemmaSenses(pairs['target'][i], lemmas2vectors)
			if col_senses and base_senses:
				base_senses = Sensembed.filter_senses_by_pos(base_senses, base_pos)
				col_senses = Sensembed.filter_senses_by_pos(col_senses, col_pos)
			else:
				continue
			if base_senses and col_senses:
				base_vec,col_vec,sim = Sensembed.closest_senses(base_senses, col_senses, M)
				# fix for potentially broken vector names
				col_lexis = col_vec.split('_bn:')[0]
				base_lexis = base_vec.split('_bn:')[0]
				col_bnsyn = 'bn:'+col_vec.split('_bn:')[1] # fix for synsets starting with _
				base_bnsyn = 'bn:'+base_vec.split('_bn:')[1]
				if col_bnsyn in shared_vocab and col_bnsyn.endswith(col_pos):
					outf_disambiguated.write(str(base_vec)+'|'+str(col_bnsyn)+'|'+str(sim)+'\n')
					all_train_vecs['base_sensembed_vector'].append(M[base_vec])
					all_train_vecs['collocate_vector'].append(shared_model[col_bnsyn])
				if col_lexis in shared_vocab:
					outf_disambiguated.write(str(base_vec)+'|'+str(col_lexis)+'|'+str(sim)+'\n')
					all_train_vecs['base_sensembed_vector'].append(M[base_vec])
					all_train_vecs['collocate_vector'].append(shared_model[col_lexis])

		outf_disambiguated.close()
		matrix_train_source = all_train_vecs['base_sensembed_vector']
		matrix_train_target_bnsyn = all_train_vecs['collocate_vector']

		from sklearn import linear_model
		clf = linear_model.LinearRegression()
		#train = np.array(matrix_train_source)
		#test = np.array(matrix_train_target_bnsyn)
		clf.fit(np.array(matrix_train_source),np.array(matrix_train_target_bnsyn))

		#print len(matrix_train_source),len(matrix_train_target_bnsyn)
		#print 'Generat	ing translation matrix'
		# Matrix W is given in  http://stackoverflow.com/questions/27980159/fit-a-linear-transformation-in-python
		#tm_full = np.linalg.pinv(matrix_train_source).dot(matrix_train_target_bnsyn).T
		all_base_cpts = [wn.synset(elm.strip()) for elm in open(base_cpts, 'r').readlines()]
		types_of_all = []
		for base_cpt in all_base_cpts:
			types_of_all.extend(types_of(base_cpt))
		print 'Looking up collocates for these many base synsets ',len(types_of_all)
		outf_col = open('../output/'+fl+'_sensembed_WORDNET.txt','w')
		M.init_sims(replace=True)
		shared_model.init_sims(replace=True)
		for n in types_of_all:
			print 'Getting candidates for: ',n
			try:
				wn_cands,surface_cands = get_collocates_syn(n,base_pos)
				filt = filter_candidates(wn_cands)
				for s in filt:
					for c in filt[s]:
						if col_pos == 'a':
							if c[0].pos() == 'a' or c[0].pos() == 's':
								outf_col.write(n.name()+'\t'+c[0].name()+'\t'+str(c[1])+'\n')
						else:
							if c[0].pos() == col_pos:
								outf_col.write(n.name()+'\t'+c[0].name()+'\t'+str(c[1])+'\n')
				outf_col.write('\n')
			except KeyError:
				print '<<ERROR>> in synset:',n
				pass
		outf_col.close()

